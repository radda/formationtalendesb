Composants utilisés : 

cTimer : https://help.talend.com/fr-FR/components/7.3/mediation-orchestration/ctimer
cSQLConnection : https://help.talend.com/fr-FR/components/8.0/mediation-database/csqlconnection
cSQL : https://help.talend.com/fr-FR/components/8.0/mediation-database/csql


Repo maven JDBC Snowflake : 
(Principal) https://repo1.maven.org/maven2/net/snowflake/snowflake-jdbc/
(3.16.0) https://repo1.maven.org/maven2/net/snowflake/snowflake-jdbc/3.16.0/

Configuration JDBC Snowflake : 
https://docs.snowflake.com/en/developer-guide/jdbc/jdbc-configure
Exemple : jdbc:snowflake://myorganization-myaccount.snowflakecomputing.com/?user=Admin&warehouse=COMPUTE_WH&db=ICOMMERCE&schema=I_OPE


Scripts utilisés dans les composants
Suppression de la table TB_CATEGORIE<br><b>                          __UNIQUE_NAME__</b>
"DROP TABLE IF EXISTS TB_CATEGORIE ;"


Création de la table TB_CATEGORIE<br><b>                      __UNIQUE_NAME__</b>
"CREATE TABLE TB_CATEGORIE (
CD_CATEGORIE VARCHAR(50) NOT NULL,
LB_CATEGORIE VARCHAR(100) NOT NULL,
CONSTRAINT TB_CATEGORIE_PKEY PRIMARY KEY(CD_CATEGORIE)
);"


Insertion des données dans la table TB_CATEGORIE<br><b>                                  __UNIQUE_NAME__</b>
"INSERT INTO TB_CATEGORIE 
VALUES
('C001','Technologie'),
('C002','Mobilier'),
('C003','Fournitures de bureau');"